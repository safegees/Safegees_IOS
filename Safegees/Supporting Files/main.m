//
//  main.m
//  Safegees
//
//  Created by Victor Purcallas Marchesi on 26/9/15.
//  Copyright © 2015 Safegees. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
