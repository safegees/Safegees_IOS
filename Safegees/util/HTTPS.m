
#import "HTTPS.h"

@implementation HTTPS
{
    BOOL finished;
    NSURLConnection *connection;
    NSHTTPURLResponse *response;
    NSMutableData *data;
    NSError *error;
}

-(id) init {
    if ((self = [super init]) != nil) {
        finished = NO;
    }
    return self;
}

-(void) connectWithRequest:(NSURLRequest*)request {
    connection = [NSURLConnection connectionWithRequest:request delegate: self];
    NSRunLoop* loop = [NSRunLoop currentRunLoop];
    while (finished == NO && [loop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture] ]);
}

-(NSURLConnection *)connection{
    return connection;
}
-(NSHTTPURLResponse *)response{
    return response;
}
-(NSData *)data{
    return data;
}
-(NSError *)error{
    return error;
}

#pragma mark --
#pragma mark Delegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)_response {
    response = (NSHTTPURLResponse*)_response;
    //NSLog(@"000>%@", _response);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)_data {
    if (data == nil) data = [NSMutableData dataWithData:_data];
    else [data appendData: _data];
    //NSLog(@"--->: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    finished = YES;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)_error {
    error = _error;
    finished = YES;
    //NSLog(@"err->%@", error);
}

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    NSURLProtectionSpace * protectionSpace = [challenge protectionSpace];
    NSURLCredential* credentail = [NSURLCredential credentialForTrust:[protectionSpace serverTrust]];
    [[challenge sender] useCredential:credentail forAuthenticationChallenge:challenge];
}

@end