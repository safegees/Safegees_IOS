//
//  CommonUtils.h
//  CPRC
//
//  Created by Victor Purcallas Marchesi on 8/9/15.
//  Copyright (c) 2015 Safegees. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonUtils : NSObject

+ (BOOL) isiPad;

+ (BOOL) isIphoneScreenWidthSmall;
+ (BOOL) isIphoneScreenWidthMedium;
+ (BOOL) isIphoneScreenWidthLong;

@end
