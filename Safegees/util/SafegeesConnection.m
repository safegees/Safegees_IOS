//
//  SafageesConnection
//
//  This class acts as a connection with server manager
//  It get/posts the neccesary info from/in the server
//
//  Created by Victor Purcallas Marchesi on 20/6/15.
//  Copyright (c) 2015 Safegees. All rights reserved.
//

#import "SafegeesConnection.h"
#import "Strings.h"
#import "ConfigurationManager.h"


//Direction
static NSString* WEB_BASE =@"https://safegees.appspot.com/v1/";
//static NSString* GET_POSITION =@"position/";
static NSString* GET_POSITION =@"user/position/";
static NSString* GET_LOGIN =@"user/login/";
static NSString* SET_POSITION =@"user/position/";
//static NSString* SET_POSITION =@"position/";
static NSString* OWN_REGISTER =@"user/";
static NSString* AUTHORIZE_USER =@"user/authorized/";
static NSString* GET_POINTS_OF_INTEREST =@"map";
//POST keys
static NSString* KEY_HEADER_AUTHORIZED = @"auth";
static NSString* POST_KEY_BODY_EMAIL=@"email";
static NSString* POST_KEY_BODY_PASSWORD=@"password";
static NSString* POST_KEY_BODY_AUTHORIZED_EMAIL=@"authorized_email";
static NSString* POST_KEY_BODY_POSITION=@"position";

//TEST
//static NSString* OWNER = @"alvaro@gmail.com";
//static NSString* OWNER_PASS = @"password";


@implementation SafegeesConnection

//  This is a list of Server services:
//
//  1. User register
//  POST
//  Url: https://safegees.appspot.com/v1/user/
//  Body:
//      email: owner@mail.com
//      password: password
//  ---------------------
//  2. Allow contact to see my position (and info)
//  POST
//  Url: https://safegees.appspot.com/v1/user/authorized/
//  Header:
//      auth:owner@mail.com:password
//  Body:
//      authorized_email: contact@mail.com
//  -----------------------
//  3. Update my position
//  POST
//  Url: https://safegees.appspot.com/v1/position
//  Header:
//      auth:owner@mail.com:password
//  Body:
//      position: latitud, longitud
//  -----------------------
//  4. Get contacts position (and info)
//  GET
//  Url: https://safegees.appspot.com/v1/position/
//  Header:
//      auth: owner@mail.com:password
//  -----------------------
//  5. See points of interest on map
//  GET
//  Url: https://safegees.appspot.com/v1/map/
//  -----------------------


#pragma mark - POST on server

//Register application user
+ (bool) registerUserWithEmail:(NSString*) email andWithPassword:(NSString*) password{
    NSString *stringURL = [WEB_BASE stringByAppendingString:OWN_REGISTER];
    NSURL *url = [NSURL URLWithString: stringURL];
    NSString* post = [NSString stringWithFormat:@"%@=%@&%@=%@", POST_KEY_BODY_EMAIL, email, POST_KEY_BODY_PASSWORD, password];
    NSMutableURLRequest *request = [self buildPOSTRequest:post url:url];
    return [self sendToServer:request];
}

//Allows contact to see the user location
+ (bool) addNewContact:(NSString*) email{
    NSString *stringURL = [WEB_BASE stringByAppendingString:AUTHORIZE_USER];
    NSURL *url = [NSURL URLWithString: stringURL];
    NSString* post = [NSString stringWithFormat:@"%@=%@", POST_KEY_BODY_AUTHORIZED_EMAIL,email];
    NSMutableURLRequest *request = [self buildPOSTRequest:post url:url];
    //NSMutableURLRequest *mutableRequest = [request mutableCopy];
    [request addValue:[NSString stringWithFormat:@"%@:%@",[ConfigurationManager getString:USER_NAME], [ConfigurationManager getString:USER_PASSWORD]] forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    //NSLog(@"Mutable request %@ body %@  header %@", mutableRequest.description, newStr, mutableRequest.allHTTPHeaderFields);
    
    return [self sendToServer:request];
}

//Update user position in server
+(BOOL) updatePositionWithLatitude:(double) latitude andLongitude:(double) longitude{
    NSString *stringURL = [WEB_BASE stringByAppendingString:SET_POSITION];
    NSURL *url = [NSURL URLWithString: stringURL];
    NSString* post = [NSString stringWithFormat:@"%@=%@", POST_KEY_BODY_POSITION,[NSString stringWithFormat:@"%f,%f", latitude, longitude]];
    NSMutableURLRequest *request = [self buildPOSTRequest:post url:url];
    //NSMutableURLRequest *mutableRequest = [request mutableCopy];
    [request addValue:[NSString stringWithFormat:@"%@:%@",[ConfigurationManager getString:USER_NAME], [ConfigurationManager getString:USER_PASSWORD]] forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    return [self sendToServer:request];
}

#pragma mark - GET from server


//Get contacts info (including position)
+ (void) getFriendsPost{
    NSString *stringURL = [WEB_BASE stringByAppendingString:GET_POSITION];
    NSString *json = [self getServerDataJSON:stringURL];
    if (json != nil){
        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:NULL];
        NSLog(@"%@", jsonObject);
        [ConfigurationManager putNSDictionary:FRIENDS_JSON value:jsonObject];
    }
}

//Get interest points (non contact info)
+ (void) getPointsOfInterest{
    
    NSString *stringURL = [WEB_BASE stringByAppendingString:GET_POINTS_OF_INTEREST];
    NSString *json = [self getServerDataJSON:stringURL];
    //TEST LOAD LOCAL JSON
    //    NSString *str=[[NSBundle mainBundle] pathForResource:@"fake_safegees" ofType:@"json"];
    //    NSData *fileData = [NSData dataWithContentsOfFile:str];
    //
    //    NSString *json=[[NSString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
    if (json != nil){
        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:NULL];
        NSLog(@"%@", jsonObject);
        [ConfigurationManager putNSDictionary:POINTS_OF_INTEREST value:jsonObject];
    }
}


#pragma mark - Common methods

// Check if user is login

+ (BOOL) getLoginStatusWithEmail:(NSString*) email andWithPassword:(NSString*) password {
    NSString *stringURL = [WEB_BASE stringByAppendingString:GET_LOGIN];
    NSURL *url = [NSURL URLWithString: stringURL];
    
    NSString* post = [NSString stringWithFormat:@"email=%@&password=%@", email,password];
    NSMutableURLRequest *request = [self buildPOSTRequest:post url:url];
    
    HTTPS *https = [[HTTPS alloc] init];
    [https connectWithRequest:request];
    NSData *responseData=[https data] ;
    NSError *err=[https error];
    NSHTTPURLResponse *httpResponse = [https response];
    if (err!= nil && err.code != 200){
        NSLog(@"Error code : %li", err.code);
        return false;
    }
    if (httpResponse.statusCode != 200) {
        NSLog(@"Status error : %li || Response data: %@", (long)httpResponse.statusCode, responseData.description);
        return false;
    }
    return true;
}



// Get json from server
+ (NSString *)getServerDataJSON:(NSString *)stringURL {
    NSURL *url = [NSURL URLWithString: stringURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    
    NSMutableURLRequest *mutableRequest = [request mutableCopy];
    [mutableRequest addValue:[NSString stringWithFormat:@"%@:%@",[ConfigurationManager getString:USER_NAME], [ConfigurationManager getString:USER_PASSWORD]] forHTTPHeaderField:KEY_HEADER_AUTHORIZED];
    
    // Now set our request variable with an (immutable) copy of the altered request
    request = [mutableRequest copy];
    
    HTTPS *https = [[HTTPS alloc] init];
    [https connectWithRequest:request];
    NSData *responseData=[https data] ;
    NSError *err=[https error];
    NSHTTPURLResponse *httpResponse = [https response];
    if (err!= nil && err.code != 200){
        NSLog(@"Error code");
    }
    if (httpResponse.statusCode != 200) {
        NSLog(@"Status error : %li", (long)httpResponse.statusCode);
    }
    
    
    NSString *json=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    return json;
}

//Build the basic post
+ (NSMutableURLRequest *)buildPOSTRequest:(NSString *)post url:(NSURL *)url {
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    [request setTimeoutInterval:10];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPBody:postData];
    return request;
}

//Send request to server (Catching error responses)
+ (BOOL)sendToServer:(NSMutableURLRequest *)request {
    // Now set our request variable with an (immutable) copy of the altered request
    //request = [mutableRequest copy];
    HTTPS *https = [[HTTPS alloc] init];
    [https connectWithRequest:request];
    NSData *responseData=[https data] ;
    NSError *err=[https error];
    NSHTTPURLResponse *httpResponse = [https response];
    NSLog(@"%@",  [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] );
    if (err!= nil && err.code != 201){
        NSLog(@"Error code");
        return NO;
    }
    if (httpResponse.statusCode != 201) {
        NSLog(@"Http response error : %li", (long)httpResponse.statusCode);
        return NO;
    }
    return YES;
}

//Replace json null values by text
+ (NSMutableDictionary *)dictionaryByReplacingNullsWithStrings:(NSDictionary*) dict {
    const NSMutableDictionary *replaced = [dict mutableCopy];
    const id nul = [NSNull null];
    const NSString *blank = @" ";
    
    for(NSString *key in dict) {
        const id object = [dict objectForKey:key];
        if(object == nul) {
            [replaced setObject:blank
                         forKey:key];
        }
    }
    
    return [replaced copy];
}

#pragma mark - Check connection

+ (bool) isNetworkAvailable{
    Reachability *internetReach = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    switch (netStatus)
    {
        case ReachableViaWWAN:
        {
            break;
        }
        case ReachableViaWiFi:
        {
            break;
        }
        case NotReachable:
        {
            return false;
        }
            
    }
    return true;
}



@end
