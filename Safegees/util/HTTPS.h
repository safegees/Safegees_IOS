

#import <Foundation/Foundation.h>

@interface HTTPS : NSObject <NSURLConnectionDelegate>

@property (nonatomic) BOOL ssl;

-(void) connectWithRequest:(NSURLRequest*)request;
-(NSURLConnection *)connection;
-(NSHTTPURLResponse *)response;
-(NSData *)data;
-(NSError *)error;

@end
