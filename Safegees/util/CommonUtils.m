//
//  CommonUtils.m
//  CPRC
//
//  Created by Victor Purcallas Marchesi on 8/9/15.
//  Copyright (c) 2015 Safegees. All rights reserved.
//

#import "CommonUtils.h"
#import <UIKit/UIKit.h>


@implementation CommonUtils

+ (BOOL) isiPad{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return YES;
    }
    return NO;
}

+ (BOOL) isIphoneScreenWidthSmall{
    NSLog(@"%f",[[UIScreen mainScreen] bounds].size.width);
    if ([[UIScreen mainScreen] bounds].size.width == 320.0f){
        return true;
    }
    return false;
}

+ (BOOL) isIphoneScreenWidthMedium{
    if (![CommonUtils isiPad] && [[UIScreen mainScreen] bounds].size.width == 375.0f){
        return true;
    }
    return false;
}

+ (BOOL) isIphoneScreenWidthLong{
    if (![CommonUtils isiPad] && [[UIScreen mainScreen] bounds].size.width == 414.0f){
        return true;
    }
    return false;
}

@end
