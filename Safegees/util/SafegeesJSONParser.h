//
//  SafegeesJSONParser.h
//  Safegees
//
//  Created by Victor Purcallas Marchesi on 26/9/15.
//  Copyright © 2015 Safegees. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SafegeesJSONParser : NSObject

    +(NSArray*) getArrayOfUsers;

@end
