//
//  UnExplConnection.h
//  unexpl
//
//  Created by Victor Purcallas Marchesi on 20/6/15.
//  Copyright (c) 2015 United Explanations. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTTPS.h"
#import "Reachability.h"
#import "ConfigurationManager.h"
#import "Strings.h"


@interface SafegeesConnection : NSObject

+ (bool) isNetworkAvailable;
+ (void) getFriendsPost;
+ (void) getPointsOfInterest;
+ (BOOL) getLoginStatusWithEmail:(NSString*) email andWithPassword:(NSString*) password;
+ (bool) addNewContact:(NSString*) email;
+ (NSMutableURLRequest *)buildPOSTRequest:(NSString *)post url:(NSURL *)url ;
+ (BOOL) updatePositionWithLatitude:(double) latitude andLongitude:(double) longitude;



@end
