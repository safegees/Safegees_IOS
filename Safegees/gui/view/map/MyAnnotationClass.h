//
//  MyAnnotationClass.h
//  Safegees
//
//  Created by Victor Purcallas Marchesi on 27/9/15.
//  Copyright © 2015 Safegees. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MyAnnotationClass : NSObject <MKAnnotation> {
    NSString *_name;
    NSString *_description;
    CLLocationCoordinate2D _coordinate;
}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *descr;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;

-(id) initWithCoordinate:(CLLocationCoordinate2D) coordinate;

@end;