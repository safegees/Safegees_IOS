//
//  MapViewController.m
//  Safegees
//
//  Created by Victor Purcallas Marchesi on 26/9/15.
//  Copyright © 2015 Safegees. All rights reserved.
//

#import "MapViewController.h"
#import "ConfigurationManager.h"
#import "Strings.h"
#import "MyAnnotationClass.h"
#import "SafegeesConnection.h"
#import "MainViewController.h"


@interface MapViewController (){
CLLocation* crnLoc;
bool locationChecked;

}

@end

@implementation MapViewController


@synthesize coordinate = _coordinate;
@synthesize locationManager = locationManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    locationChecked = false;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    NSString *version = [[UIDevice currentDevice] systemVersion];
    if([version floatValue] >= 8.0){
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }
    }
    [self.locationManager startUpdatingLocation];
//
//    CLLocationDegrees lat = 41.9799504;
//    CLLocationDegrees lon = 2.1728926;
//    CLLocationCoordinate2D coord =  CLLocationCoordinate2DMake(lat, lon);
//    NSString* peaceHackBcn = @"#peacehack bcn";
//    [self setAnotationToMap:coord andWIthTitle:peaceHackBcn];
//    
//    
//    lat = 41.3799504;
//    lon = 2.1728926;
//    coord =  CLLocationCoordinate2DMake(lat, lon);
//    NSString* punto = @"#prueba";
//    [self setAnotationToMap:coord andWIthTitle:punto];
    
    //[self.mapView setShowsUserLocation:YES];

}
//-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
//    
//    UIAlertView *errorAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"There was an error retrieving your location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//    
//    [errorAlert show];
//    
//    NSLog(@"Error: %@",error.description);
//    
//}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations

{
    if(!locationChecked){
        NSLog(@"%@", [locations lastObject]);
        crnLoc = [locations lastObject];
        locationChecked = true;
        [self sendUserPositionToServer];
    }
}

-(void) sendUserPositionToServer{
    if([SafegeesConnection isNetworkAvailable]){
        CLLocationDegrees latitude = crnLoc.coordinate.latitude;
        CLLocationDegrees longitude = crnLoc.coordinate.longitude;
        [SafegeesConnection updatePositionWithLatitude:latitude andLongitude:longitude];
    }
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.mapView setShowsUserLocation:YES];
    
    // Do any additional setup after loading the view.
    //self.
    //self.mapView.showsUserLocation = YES;
    
    [self setFriendAnotations];
    [self setPointsOfInterest];
    
}

-(IBAction)logout:(id)sender{
    [ConfigurationManager clearAll];
    MainViewController* mvc = (MainViewController*) self.parentViewController;
    
    [self dismissViewControllerAnimated:YES completion:^{
      [mvc start];
    }];
}

- (void)requestAlwaysAuthorization
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    // If the status is denied or only granted for when in use, display an alert
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusDenied) {
        NSString *title;
        title = (status == kCLAuthorizationStatusDenied) ? @"Location services are off" : @"Background location is not enabled";
        NSString *message = @"To use background location you must turn on 'Always' in the Location Services Settings";
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"Settings", nil];
        [alertView show];
    }
    // The user has not enabled any location services. Request background authorization.
    else if (status == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestAlwaysAuthorization];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        // Send the user to the Settings for this app
        NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:settingsURL];
    }
}

-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.mapView setShowsUserLocation:NO];
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    NSLog(@"didUpdateUserLocation");
    
    double miles = 10.0;
    
    //Instead of manually calculating span from miles to degrees,
    //use MKCoordinateRegionMakeWithDistance function...
    //Just need to convert miles to meters.
    CLLocationDistance meters = miles * 1609.344;
    MKCoordinateRegion region2 = MKCoordinateRegionMakeWithDistance
    (userLocation.coordinate, meters, meters);
    
    [self.mapView setRegion:region2 animated:YES];

}

- (void) setPointsOfInterest{
    NSDictionary* pois = [ConfigurationManager getNSDictionary:POINTS_OF_INTEREST];
    for (NSDictionary *poi in pois) {
        NSString* coords = [poi objectForKey:@"position"];
        NSArray *array = [coords componentsSeparatedByString:@","];
        NSLog(@"%@",poi);
        CLLocationDegrees lat = [[array objectAtIndex:0] doubleValue];
        CLLocationDegrees lon = [[array objectAtIndex:1] doubleValue];
        CLLocationCoordinate2D coord =  CLLocationCoordinate2DMake(lat, lon);
        NSString* description = [poi objectForKey:@"description"];
        [self setAnotationInfoToMap:coord andWIthTitle:description];
    }
}


- (void) setFriendAnotations{
    NSDictionary* friends = [ConfigurationManager getNSDictionary:FRIENDS_JSON];
    for (NSDictionary *friend in friends) {
        NSString* coords = [friend objectForKey:@"position"];
        NSArray *array = [coords componentsSeparatedByString:@","];
        NSLog(@"%@",friend);
        CLLocationDegrees lat = [[array objectAtIndex:0] doubleValue];
        CLLocationDegrees lon = [[array objectAtIndex:1] doubleValue];
        CLLocationCoordinate2D coord =  CLLocationCoordinate2DMake(lat, lon);
        NSString* email = [friend objectForKey:@"email"];
       [self setAnotationToMap:coord andWIthTitle:email];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
//{
//    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
//    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
//}

//-(CLLocationCoordinate2D)coordinate {
//
// CLLocationDegrees lat = 38.3799504;
// CLLocationDegrees lon = 2.1728926;
//    CLLocationCoordinate2D coord =  CLLocationCoordinate2DMake(lat, lon);
//    return coord;
//}


-(void) setAnotationToMap:(CLLocationCoordinate2D) coordinate2D andWIthTitle:(NSString*) title{

    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:coordinate2D];
    [annotation setTitle:title]; //You can set the subtitle too
    [self.mapView addAnnotation:annotation];
}

-(void) setAnotationInfoToMap:(CLLocationCoordinate2D) coordinate2D andWIthTitle:(NSString*) title{
    
    MyAnnotationClass *annotation = [[MyAnnotationClass alloc] initWithCoordinate:coordinate2D];
    //[annotation setCoordinate:coordinate2D];
    [annotation setName:title]; //You can set the subtitle too
    annotation.title = title;
    [self.mapView addAnnotation:annotation];
}



-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *pinView = nil;
    if(annotation != self.mapView.userLocation)
    {
        static NSString *defaultPinID = @"com.invasivecode.pin";
        pinView = (MKAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil )
            pinView = [[MKAnnotationView alloc]
                       initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        
        //pinView.pinColor = MKPinAnnotationColorGreen;
        pinView.canShowCallout = YES;
        //pinView.animatesDrop = YES;
        
        
        if ([[annotation class] isSubclassOfClass:[MyAnnotationClass class]]){
            UIImage* testImage = [UIImage imageNamed:@"default_info_mini"];
            //MyAnnotationClass* myAnot = (MyAnnotationClass*) annotation;
            pinView.image = testImage;
        }else{
           UIImage*  testImage =[UIImage imageNamed:@"default_user_mini"];
           pinView.image = testImage;
        }
    }
//    else {
//        [self.mapView.userLocation setTitle:@"I am here"];
//    }
    return pinView;
}

- (BOOL) validEmail:(NSString*) emailString {
    if([emailString length]==0){
        return NO;
    }
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    NSLog(@"%lu", (unsigned long)regExMatches);
    
    if (regExMatches == 0) {
        return NO;
    }
    return YES;
}

-(IBAction)update:(id)sender{
    if ([SafegeesConnection isNetworkAvailable]){
        [SafegeesConnection getFriendsPost];
        [SafegeesConnection getPointsOfInterest];
        [self sendUserPositionToServer];
        [self setFriendAnotations];
        [self setPointsOfInterest];
    }
}

-(void) goToAddContact{
    [self performSegueWithIdentifier:@"addcontact" sender:self];
}
-(void) goToContact{
    [self performSegueWithIdentifier:@"contacts" sender:self];
}
-(void) goToNews{
    [self performSegueWithIdentifier:@"news" sender:self];
}
-(void) goToProfile{
    [self performSegueWithIdentifier:@"profile" sender:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
// Set white status bar
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
