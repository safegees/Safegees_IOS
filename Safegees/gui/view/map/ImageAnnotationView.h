//
//  ImageAnnotationViewController.h
//  Safegees
//
//  Created by Victor Purcallas Marchesi on 26/9/15.
//  Copyright © 2015 Bayer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ImageAnnotationView : NSObject<MKAnnotation> {
}

@property (nonatomic, retain) NSString *mPinColor;

@end
