//
//  ImageAnnotationViewController.m
//  Safegees
//
//  Created by Victor Purcallas Marchesi on 26/9/15.
//  Copyright © 2015 Bayer. All rights reserved.
//


#import "ImageAnnotationView.h"

@interface ImageAnnotationView ()

@end

@implementation ImageAnnotationView

@synthesize mPinColor = mPinColor;

- (NSString *)pincolor{
    return mPinColor;
}

- (void) setpincolor:(NSString*) String1{
    mPinColor = String1;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
