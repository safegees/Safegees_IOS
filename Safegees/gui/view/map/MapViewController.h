//
//  MapViewController.h
//  Safegees
//
//  Created by Victor Purcallas Marchesi on 26/9/15.
//  Copyright © 2015 Safegees. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MapViewController : UIViewController <MKMapViewDelegate, MKAnnotation, CLLocationManagerDelegate>

@property (nonatomic, strong) IBOutlet MKMapView* mapView;
@property (nonatomic, strong) CLLocationManager *locationManager;


-(IBAction)update:(id)sender;
-(void) goToAddContact;
-(void) goToContact;
-(void) goToNews;
-(void) goToProfile;


@end
