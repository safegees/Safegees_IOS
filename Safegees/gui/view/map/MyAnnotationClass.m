//
//  MyAnnotationClass.m
//  Safegees
//
//  Created by Victor Purcallas Marchesi on 27/9/15.
//  Copyright © 2015 Safegees. All rights reserved.
//

#import "MyAnnotationClass.h"

@interface MyAnnotationClass () {}

@end

@implementation MyAnnotationClass

-(id) initWithCoordinate:(CLLocationCoordinate2D) coordinate{
    self = [super init];
    if (self){
        _coordinate = coordinate;
    
    }
    return self;
}

-(CLLocationCoordinate2D)coordinate { return _coordinate; }

@end
