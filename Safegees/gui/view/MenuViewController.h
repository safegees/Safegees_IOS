//
//  MenuViewController.h
//  Safegees
//
//  Created by Victor Purcallas Marchesi on 27/9/15.
//  Copyright © 2015 Safegees. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIView *mainView;
@property (nonatomic, strong) IBOutlet UIView *viewBackground;

-(void)setDarkBackground;
-(IBAction)close:(id)sender;

@end
