//
//  RegisterViewController.h
//  Safegees
//
//  Created by Victor Purcallas Marchesi on 27/9/15.
//  Copyright © 2015 Safegees. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddNewFriendViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITextField* tfEmail;

-(IBAction)addFriend:(id)sender;
-(IBAction)close:(id)sender;
@end
