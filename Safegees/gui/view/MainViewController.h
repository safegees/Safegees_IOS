//
//  MainViewController.h
//  Safegees
//
//  Created by Victor Purcallas Marchesi on 18/11/15.
//  Copyright © 2015 Safegees. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController

- (void)start;

@end
