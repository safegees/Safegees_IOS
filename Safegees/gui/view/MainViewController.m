//
//  MainViewController.m
//  Safegees
//
//  Created by Victor Purcallas Marchesi on 18/11/15.
//  Copyright © 2015 Safegees. All rights reserved.
//

#import "MainViewController.h"
#import "ConfigurationManager.h"
#import "Strings.h"
#import "SafegeesConnection.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)start {
    if ([ConfigurationManager contains:IS_LOGED]){
        if([ConfigurationManager getBool:IS_LOGED]){
            if ([SafegeesConnection isNetworkAvailable]){
                //Internet and logged
                [SafegeesConnection getFriendsPost];
                [SafegeesConnection getPointsOfInterest];
                [self performSegueWithIdentifier:@"webView" sender:self];
            }else{
                //No internet previously logged
                [self performSegueWithIdentifier:@"webView" sender:self];
            }
        }else{
            //Loged is no
            [self performSegueWithIdentifier:@"login" sender:self];
        }
    }else{
        //First time on aplication
        [self performSegueWithIdentifier:@"login" sender:self];
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self start];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// Set white status bar
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
