//
//  MenuViewController.m
//  Safegees
//
//  Created by Victor Purcallas Marchesi on 27/9/15.
//  Copyright © 2015 Safegees. All rights reserved.
//

#import "MenuViewController.h"
#import "MapViewController.h"

static int STATIC_IPAD_HORIZONTAL_POS = 195;

@interface MenuViewController (){
    CGPoint bottomOffset;
}

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
//    UISwipeGestureRecognizer* swipeRightGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRightFrom:)];
//    swipeRightGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
//    
//    [self.view addGestureRecognizer:swipeRightGestureRecognizer];
    
    self.mainView.layer.masksToBounds = NO;
    self.mainView.layer.shadowOffset = CGSizeMake(-50, 0);
    self.mainView.layer.shadowRadius = 7;
    self.mainView.layer.shadowOpacity = 0.2;
    CGRect bounds = self.mainView.bounds;
    bounds.size.height *= 2;
    self.mainView.layer.shadowPath = [UIBezierPath bezierPathWithRect:bounds].CGPath;
}

-(void) dismiss:(NSString*)nextSegue {
    
    CGRect frame = self.mainView.frame;
    frame.origin.x -= STATIC_IPAD_HORIZONTAL_POS * 2;
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         if (nextSegue == nil) {
                             [self.view setAlpha:1];
                         }
                         [self.mainView setFrame:frame];
                         [self.viewBackground setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.0]];
                     }
                     completion:^(BOOL finished){
                         [self removeFromParentViewController];
                         [self.view removeFromSuperview];
                         [self dismissViewControllerAnimated:NO completion:^{
                             //if (nextSegue != nil) {
                             [self.parentViewController performSegueWithIdentifier:nextSegue sender: self];
                             //}
                         }];
                     }];
}

-(IBAction)close:(id)sender{
    [self dismiss: nil];
}

-(IBAction)goToProfile:(id)sender{
    [self dismiss: nil];
    MapViewController* map = (MapViewController*) self.parentViewController;
    [map goToProfile];
    
}
-(IBAction)goToNews:(id)sender{
    [self dismiss: nil];
    MapViewController* map = (MapViewController*) self.parentViewController;
    [map goToNews];
    
}
-(IBAction)goToContacts:(id)sender{
    [self dismiss: nil];
    MapViewController* map = (MapViewController*) self.parentViewController;
    [map goToContact];
    
}
-(IBAction)goToAddContact:(id)sender{
    [self dismiss: nil];
    MapViewController* map = (MapViewController*) self.parentViewController;
    [map goToAddContact];
    
}

-(void)setDarkBackground {
    NSLog(@"setDarkBackground");
    [self.viewBackground setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.0]];
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         [self.viewBackground setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
                     }
                     completion:nil];
    
}
// Set white status bar
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
