//
//  RegisterViewController.m
//  Safegees
//
//  Created by Victor Purcallas Marchesi on 26/9/15.
//  Copyright © 2015 Safegees. All rights reserved.
//

#import "LoginViewController.h"
#import "SafegeesConnection.h"
#import "SafegeesJSONParser.h"
#import "ConfigurationManager.h"
#import "Strings.h"
#import "MainViewController.h"


@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)sendLogin:(id)sender{
    
    if([SafegeesConnection getLoginStatusWithEmail:self.name.text andWithPassword:self.pass.text]){
        [ConfigurationManager putStringInKey:USER_NAME value:self.name.text];
        [ConfigurationManager putStringInKey:USER_PASSWORD value:self.pass.text];
        [ConfigurationManager putBoolInKey:IS_LOGED value:YES];
        MainViewController* mainVC = (MainViewController*)self.parentViewController;
        [mainVC start];
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @""
                                                       message: @"The user isn't exists"
                                                      delegate: self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        [alert show];
    }
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

// Set white status bar
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
