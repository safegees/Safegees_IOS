//
//  RegisterViewController.h
//  Safegees
//
//  Created by Victor Purcallas Marchesi on 26/9/15.
//  Copyright © 2015 Safegees. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic,strong) IBOutlet UITextField* name;
@property (nonatomic, strong) IBOutlet UITextField* pass;

@end
