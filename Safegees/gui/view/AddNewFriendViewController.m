//
//  RegisterViewController.m
//  Safegees
//
//  Created by Victor Purcallas Marchesi on 27/9/15.
//  Copyright © 2015 Safegees. All rights reserved.
//

#import "AddNewFriendViewController.h"

#import "SafegeesConnection.h"

@interface AddNewFriendViewController ()

@end

@implementation AddNewFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)close:(id)sender{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


-(IBAction)addFriend:(id)sender{

    bool isAdded = [SafegeesConnection addNewContact:self.tfEmail.text];
    [self.tfEmail resignFirstResponder];
    self.tfEmail.text = @"";
    
   
    if (isAdded){
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @""
                                                   message: @"Friend was added"
                                                  delegate: self
                                         cancelButtonTitle:nil
                                         otherButtonTitles:@"OK",nil];
    
    [alert setTag:1];
    [alert show];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Opss"
                                                       message: @"Ha habido un problema, pruebe de nuevo más tarde"
                                                      delegate: self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"OK",nil];
        
        [alert setTag:1];
        [alert show];

    
    }
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
// Set white status bar
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
