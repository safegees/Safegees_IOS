
#import "SlideLeftMenuCustomSegue.h"
#import "MenuViewController.h"
#import "CommonUtils.h"

static int STATIC_IPAD_HORIZONTAL_POS = 195;
static int STATIC_IPAD_ORIGIN_POS = 573;

static int STATIC_IPHONE_SMALL_HORIZONTAL_POS = 0;
static int STATIC_IPHONE_SMALL_ORIGIN_POS = 320;

static int STATIC_IPHONE_MEDIUM_HORIZONTAL_POS = 0;
static int STATIC_IPHONE_MEDIUM_ORIGIN_POS = 375;

static int STATIC_IPHONE_LONG_HORIZONTAL_POS = 0;
static int STATIC_IPHONE_LONG_ORIGIN_POS = 414;



@implementation SlideLeftMenuCustomSegue



- (void)perform
{
    
    int staticHorizontalPos,staticOriginPos;
    
    if ([CommonUtils isiPad]){
        staticHorizontalPos = STATIC_IPAD_HORIZONTAL_POS;
        staticOriginPos = STATIC_IPAD_ORIGIN_POS;
    }else{
    
        if ([CommonUtils isIphoneScreenWidthSmall]){
            staticHorizontalPos = STATIC_IPHONE_SMALL_HORIZONTAL_POS;
            staticOriginPos = STATIC_IPHONE_SMALL_ORIGIN_POS;
        }else if ([CommonUtils isIphoneScreenWidthLong]){
            staticHorizontalPos = STATIC_IPHONE_LONG_HORIZONTAL_POS;
            staticOriginPos = STATIC_IPHONE_LONG_ORIGIN_POS;
        }else{
            //([CommonUtils isIphoneScreenWidthMedium])
            staticHorizontalPos = STATIC_IPHONE_MEDIUM_HORIZONTAL_POS;
            staticOriginPos = STATIC_IPHONE_MEDIUM_ORIGIN_POS;
        }
    
    }
    
    
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];

    UIViewController *sourceViewController = (UIViewController *) self.sourceViewController;
    UIViewController *destinationViewController = (UIViewController *) self.destinationViewController;
    [sourceViewController.view addSubview:destinationViewController.view];
    [destinationViewController.view setFrame:sourceViewController.view.window.frame];
    
    [sourceViewController addChildViewController:destinationViewController];
    [[UIApplication sharedApplication].delegate.window addSubview:destinationViewController.view];

    
    
    CGRect frame = destinationViewController.view.frame;
    [destinationViewController.view setFrame:frame];

    
    frame.origin.x -= frame.size.width - staticHorizontalPos;
    [destinationViewController.view setFrame:frame];
    frame.origin.x += staticOriginPos;

    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         [destinationViewController.view setFrame:frame];
                     }
                     completion:^(BOOL finished){
                         
                         [UIView animateWithDuration:0.3
                                               delay:0.0
                                             options:UIViewAnimationOptionCurveLinear
                                          animations:^{
                                              if ([destinationViewController isMemberOfClass:[MenuViewController class]]){
                                                  MenuViewController* mvc = (MenuViewController*) destinationViewController;
                                                  if ([mvc respondsToSelector:@selector(setDarkBackground)]) {
                                                      [mvc performSelector:@selector(setDarkBackground) withObject:destinationViewController];
                                                  }
                                              
                                              }
                                              
                                          }
                                          completion:^(BOOL finished){
                                              [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                                          }];
                         
                         
                     }];
}

@end
