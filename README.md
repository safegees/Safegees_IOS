ATTENTION:
This developent in IOS its stoped by strategic reasons. Our major objective public use Android devices.
You can follow the android development here:
https://gitlab.com/safegees/Safegees_Android



Safegees
========



Mobile and web application that allows refugees to privately and in a secure manner geolocate people in their contact network.
This is the IOS app alpha project


Users add a contact, they accept you, and then you can keep track of where they are. 



The app also gives you “points of interest” tailored to refugees; these points of interest are provided by (vetted) NGOs working on the ground, who can upload the information through a web form. 
There is a strong emphasis on ensuring all data is secure. 
The app needs internet connection for some functions but once basic information is loaded on your phone, it can also work offline. 
Organizations could also have access to anonymized data on the movement of users registered in the app, as a way of seeing how the flow of refugees is changing live.



More info about the project: http://www.domestika.org/es/projects/220344-safegees


This is the IOS app alpha project. You can see the web project in : https://gitlab.com/safegees/safegee-web